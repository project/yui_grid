<?php

/**
 * @file
 * Theme functions for Pure CSS integration.
 */

/**
 * Implements hook_proceess_HOOK().
 */
function yui_grid_pure_preprocess_form(&$variables) {
  // Add pure form class to forms.
  $variables['element']['#attributes']['class'][] = 'pure-form';
}

/**
 * Implements hook_proceess_HOOK().
 */
function yui_grid_pure_preprocess_table(&$variables) {
  // @todo Have yet to see use-case where this class is applied to a table.
  $variables['attributes']['class'][] = 'pure-table';
}

/**
 * Implements hook_proceess_HOOK().
 */
function yui_grid_pure_preprocess_views_view_table(&$variables) {
  // Add pure table class to views tables.
  $variables['classes_array'][] = 'pure-table';
  $variables['classes_array'][] = 'pure-table-striped';
}

/**
 * Implements hook_proceess_HOOK().
 */
function yui_grid_pure_preprocess_button(&$variables) {
  // Add pure button class to buttons.
  $variables['element']['#attributes']['class'][] = 'pure-button';
}

/**
 * Implements hook_proceess_HOOK().
 */
function yui_grid_pure_preprocess_item_list(&$variables) {
  // Add pure paginator class to pager lists.
  if ($variables['attributes']['class'][0] == 'pager') {
    $variables['attributes']['class'][] = 'pure-paginator';
    foreach ($variables['items'] as &$item) {
      if ($item['class'][0] == 'pager-current') {
        $item['class'][] = 'pure-button';
        $item['class'][] = 'pure-button-active';
        break;
      }
    }
  }
}

/**
 * Implements hook_proceess_HOOK().
 */
function yui_grid_pure_preprocess_pager_link(&$variables) {
  // Add pure button class to pager links.
  $variables['attributes']['class'][] = 'pure-button';
}

/**
 * Implements hook_proceess_HOOK().
 */
function yui_grid_pure_preprocess_menu_local_tasks(&$variables) {
  // Add pure menu button classes to primary tabs.
  if (!empty($variables['primary'])) {
    foreach ($variables['primary'] as &$element) {
      $element['#link']['localized_options']['attributes']['class'][] = 'pure-button';
      if (isset($element['#active']) && $element['#active'] == TRUE) {
        $element['#link']['localized_options']['attributes']['class'][] = 'pure-button-active';
      }
    }
  }
}

/**
 * Implements hook_proceess_HOOK().
 */
function yui_grid_pure_preprocess_links(&$variables) {
  // Add pure manu classes to lists.
  $variables['attributes']['class'][] = 'pure-menu';
  if (strpos($variables['theme_hook_original'], 'menu')) {
    // Add menu class.
    $variables['attributes']['class'][] = 'pure-menu-open';
    $variables['attributes']['class'][] = 'pure-menu-horizontal';
    // Add selected (active) class by rebuilding links array with correct key.
    $links = array();
    foreach($variables['links'] as $key => $link) {
      if (strpos($key, 'active-trail')) {
        $links[$key . ' pure-menu-selected'] = $link;
      }
      else {
        $links[$key] = $link;
      }
    }
    $variables['links'] = $links;
  }
}

/**
 * Implements hook_proceess_HOOK().
 */
function yui_grid_pure_preprocess_menu_link(&$variables) {
  // Add pure class to any active-trail menu link.
  if (isset($variables['element']['#attributes']['class']) && in_array('active-trail', $variables['element']['#attributes']['class'])) {
    $variables['element']['#attributes']['class'][] = 'pure-menu-selected';
  }
}

/**
 * Overrides theme_menu_tree().
 */
function yui_grid_pure_menu_tree($variables) {
  return '<ul class="menu pure-menu">' . $variables['tree'] . '</ul>';
}

/**
 * Implements hook_css_alter().
 */
function yui_grid_pure_css_alter(&$css) {
  // Remome system menus css.
  unset($css['modules/system/system.menus.css']);
}
