Usage
=====

Use this theme as a subtheme of your own theme. Here's how:

1. Create a new theme by creating a new directory in your
   sites/all/themes folder.

2. Create a .info file in that directory with this line:

     base theme = yui_grid

3. Add regions to your new .info file. Regions are not inherited.
