<?php

/**
 * @file
 * Make the YUI Grid theme settings available.
 */

/**
 * Implements hook_proceess_HOOK().
 */
function yui_grid_process_html(&$variables) {
  $variables['yui_page_width'] = theme_get_setting('yui_page_width');
}

/**
 * Implements hook_proceess_HOOK().
 */
function yui_grid_process_page(&$variables) {
  $variables['yui_sidebar_width'] = theme_get_setting('yui_sidebar_width');
  $variables['yui_sidebar_location'] = theme_get_setting('yui_sidebar_location');
  $variables['yui_breadcrumbs'] = theme_get_setting('yui_breadcrumbs');
  $variables['yui_responsive'] = theme_get_setting('yui_responsive');
  $variables['yui_main_width'] = yui_grid_calculate_main_width();
}

/**
 * Implements hook_preproceess_HOOK().
 *
 * Make it easier to use blocks in grids.
 */
function yui_grid_preprocess_block(&$variables) {
  $variables['classes_array'][] = 'yui-u';
  if ($variables['elements']['#weight'] == 1) {
    $variables['classes_array'][] = 'first';
  }
}

/**
 * Implements hook_css_alter().
 */
function yui_grid_css_alter(&$css) {
  $normalize = theme_get_setting('yui_normalize');
  if (!$normalize) {
    // Replace entry for normalize.css with reset.css and base.css.
    $path = path_to_theme();

    $cssreset = $css[$path . '/css/cssnormalize.css'];
    $cssreset['data'] = $path . '/css/cssreset.css';
    $cssreset['weight'] = -1;
    $items[$path . '/css/cssreset.css'] = $cssreset;

    $cssbase = $css[$path . '/css/cssnormalize.css'];
    $cssbase['data'] = $path . '/css/cssbase.css';
    $items[$path . '/css/cssbase.css'] = $cssbase;

    $css = $items + $css;
    unset($css[$path . '/css/cssnormalize.css']);
  }
}

/**
 * Calculate the width of the main content element given the sidebar.
 */
function yui_grid_calculate_main_width() {
  $sidebar_width = theme_get_setting('yui_sidebar_width');
  $sidebar = theme_get_setting('yui_sidebar_location');

  if ($sidebar) {
    $split_fraction = explode('-', $sidebar_width);
    $difference = $split_fraction[1] - $split_fraction[0];
    $main_width = $difference . '-' . $split_fraction[1];
  }
  else {
    $main_width = '1';
  }

  return $main_width;
}
